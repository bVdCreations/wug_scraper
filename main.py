from read_html import extract_from
from export_excell import export_to

if "__main__" == __name__:
    export_to(file_name='exported_xl\\wug_export.xlsx', input=extract_from('html_imports\\raw_html.txt'))
